/* 
    使用者登入與驗證還是使用firebase.auth()，但是登入後還需要向Firebase索取idToken當作之後的驗證使用

    Firebase網頁教學[五]-REST篇
    http://sj82516-blog.logdown.com/posts/1079017/firebase-page-teaching-five-rest-article
*/

var firebase = require("firebase");
const user = require("./user.js")

var loginUser;

const firebase_config = {
    apiKey: "AIzaSyDKm7dm6WhXVdCvpJcJ5L-9bvV-YQ4Nl7A",
    authDomain: "myfirebase-74629.firebaseapp.com",
    databaseURL: "https://myfirebase-74629.firebaseio.com",
    projectId: "myfirebase-74629",
    storageBucket: "myfirebase-74629.appspot.com",
    messagingSenderId: "314871737177"
}

firebase.initializeApp(firebase_config);

// ==== step1. 在網頁上註冊使用者信箱和密碼 ====
// 請參考 ./doc/*.png 

// ==== step2. 登入 ====
// 注意，每次執行時，token 都會重新產生
firebase.auth()
    .signInWithEmailAndPassword(user.usrname, user.pwd)
    .catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorMessage);
    })

// ==== step3. 取得 token ====
firebase.auth()
    .onAuthStateChanged(function (user) {
        if (user) {
            loginUser = user;
            console.log("User is logined");

            // 根據使用者取得 token
            firebase
                .auth().currentUser.getIdToken(true)
                .then(idToken => {
                    loginUser.idToken = idToken;

                    console.log(`uid: ${loginUser.uid}`)
                    console.log(`token: ${loginUser.idToken}`)
                })
                .catch(err => console.log(error));

        } else {
            loginUser = null;
            console.log("User is not logined yet.");
        }
    })





