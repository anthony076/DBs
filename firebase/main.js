const chance = require("chance")()

var firebase = require("firebase");

// ==== init firebase ====
const firebase_config = {
    apiKey: "AIzaSyDKm7dm6WhXVdCvpJcJ5L-9bvV-YQ4Nl7A",
    authDomain: "myfirebase-74629.firebaseapp.com",
    databaseURL: "https://myfirebase-74629.firebaseio.com",
    projectId: "myfirebase-74629",
    storageBucket: "myfirebase-74629.appspot.com",
    messagingSenderId: "314871737177"
}

firebase.initializeApp(firebase_config);

var db = firebase.database();

// 設定 database 的讀取/寫入路徑
var ref = db.ref("/users");

// ==== 寫入 db ====
var value = {
    name: chance.name(),
    age: chance.integer({ min: 10, max: 100 }),
    city: chance.city()
}

console.log(`Generate: ${value.name}, ${value.age}, ${value.city}`)

ref.set(value);

// ==== 讀取 db ====
ref.once("value", function (snapshot) {
    console.log(snapshot.val());
});
