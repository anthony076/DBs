# Firebase Tutorial
[Cloud] 使用 nodejs 或 REST 存取 Firebase-database

- 功能一， 利用 nodejs 存取 firebase，請參閱 `main.js`
- 功能二， 利用 REST 存取 firebase，請參閱 `REST.http` (利用 vscode REST Client plugin)
<br><br>

# 利用 nodejs 存取 firebase 流程
透過 `npm start` 或 `01_run.bat`

執行前，先確定 firebase 已啟用 <br>
![](./doc/firebase_enable.png)


# 利用 REST 存取 firebase 流程
```
注意，Firebase 不提供 ACCOUNT/PASS 登入，必須取得 token 後才能利用 POST 寫入，GET 讀取則沒有限制
```

- 流程，登入取得 token -> 利用 GET 或 POST 存取
- step1，註冊信箱和密碼<br>
    啟用電子信箱登入<br>
    ![](./doc/step1_select_login_method.png)

    註冊信箱<br>
    ![](./doc/step2_register_user.png)

- step2，取得 token<br>
    執行 `getRESTtoken.js`


- step3，將 token 填入 POST url 的 auth 參數<br>
    GET request
![](./doc/result_from_GET.png)

    POST request
![](./doc/result_from_POST.png)